<?php

function getAddress($field = '')
{
	$ip = get_client_ip();
	$ipObj = new \Org\Net\IpLocation('UTFWry.dat');
	$address = $ipObj->getlocation($ip);
	$fieldList = array('ip', 'beginip', 'endip', 'country', 'area');
	if (empty($field))
	{
		return $address;
	}
	return in_array($field, $fieldList) ? $address[$field] : '';	
}

function renderQuote($floorIdArr, array $floorInfoArr)
{
	$HTML = '<blockquote class="comment-quote">';
	$floorId = array_pop($floorIdArr);
	if (!empty($floorIdArr))
	{
		$HTML .= renderQuote($floorIdArr, $floorInfoArr);
	}
	$HTML .= '<div class="comment-info clearfix"><h5 class="comment-header text-muted">';
	$HTML .= '<span class="comment-username">'.$floorInfoArr[$floorId]['user_name'].'</span>';
	$HTML .= '<span class="comment-address">来自['.$floorInfoArr[$floorId]['address'].']</span>';
	$HTML .= '<span class="comment-floor pull-right">#'.$floorInfoArr[$floorId]['position'].'</span>';
	$HTML .= '</h5><div class="comment-text lead">'.$floorInfoArr[$floorId]['content'].'</div><div class="comment-action clearfix" data-floor-id="'.$floorId.'"><div class="pull-right">';
	$HTML .= '<a class="comment-up" href="#">支持(<span class="comment-up-count">'.$floorInfoArr[$floorId]['up_count'].'</span>)</a>';
	$HTML .= '<a class="comment-down" href="#">反对(<span class="comment-down-count">'.$floorInfoArr[$floorId]['down_count'].'</span>)</a>';
	$HTML .= '<a href="#" class="comment-report">举报</a><a href="#" class="comment-reply">回复</a>';
	$HTML .= '</div></div></div></blockquote>';
	return $HTML;
}

function renderQuote2($floorIdArr, array $floorInfoArr)
{
	$floorId = array_shift($floorIdArr);
	
	$HTML = '<blockquote class="comment-quote">';
	$HTML .= '<div class="comment-info clearfix"><h5 class="comment-header text-muted">';
	$HTML .= '<span class="comment-username">'.$floorInfoArr[$floorId]['user_name'].'</span>';
	$HTML .= '<span class="comment-address">来自['.$floorInfoArr[$floorId]['address'].']</span>';
	$HTML .= '<span class="comment-floor pull-right">#'.$floorInfoArr[$floorId]['position'].'</span>';
	$HTML .= '</h5><div class="comment-text lead">'.$floorInfoArr[$floorId]['content'].'</div><div class="comment-action clearfix" data-floor-id="'.$floorId.'"><div class="pull-right">';
	$HTML .= '<a class="comment-up" href="#">支持(<span class="comment-up-count">'.$floorInfoArr[$floorId]['up_count'].'</span>)</a>';
	$HTML .= '<a class="comment-down" href="#">反对(<span class="comment-down-count">'.$floorInfoArr[$floorId]['down_count'].'</span>)</a>';
	$HTML .= '<a href="#" class="comment-report">举报</a><a href="#" class="comment-reply">回复</a>';
	$HTML .= '</div></div></div></blockquote>';
	while(!empty($floorIdArr))
	{
		$floorId = array_shift($floorIdArr);
		$HTML = '<blockquote class="comment-quote">'.$HTML;
		$HTML .= '<div class="comment-info clearfix"><h5 class="comment-header text-muted">';
		$HTML .= '<span class="comment-username">'.$floorInfoArr[$floorId]['user_name'].'</span>';
		$HTML .= '<span class="comment-address">来自['.$floorInfoArr[$floorId]['address'].']</span>';
		$HTML .= '<span class="comment-floor pull-right">#'.$floorInfoArr[$floorId]['position'].'</span>';
		$HTML .= '</h5><div class="comment-text lead">'.$floorInfoArr[$floorId]['content'].'</div><div class="comment-action clearfix" data-floor-id="'.$floorId.'"><div class="pull-right">';
		$HTML .= '<a class="comment-up" href="#">支持(<span class="comment-up-count">'.$floorInfoArr[$floorId]['up_count'].'</span>)</a>';
		$HTML .= '<a class="comment-down" href="#">反对(<span class="comment-down-count">'.$floorInfoArr[$floorId]['down_count'].'</span>)</a>';
		$HTML .= '<a href="#" class="comment-report">举报</a><a href="#" class="comment-reply">回复</a>';
		$HTML .= '</div></div></div></blockquote>';
	}
	return $HTML;
}