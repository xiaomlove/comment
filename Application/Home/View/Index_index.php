<!DOCTYPE HTML>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>嵌套评论</title>
    <script type="text/javascript" src="http://libs.baidu.com/jquery/1.9.0/jquery.js"></script>
    <link rel="stylesheet" href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css"/>
    <script type="text/javascript" src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <style type="text/css">
        .lead{margin-bottom: 0;}
        .comment-main-header{background-color:#f5f5f5;border-top: 1px solid #ddd;margin-bottom: 10px;}
        .comment-main-header span:not(:last-of-type){margin-right: 10px;}
        .comment-item{padding-bottom: 20px;background-color: hsl(85,123%,205%) !important}
        .comment-header{font-size: 13px;}
        .comment-header span:not(:last-of-type){margin-right: 10px;}
        .comment-item>div>.comment-header{margin-top: 0;padding: 10px 0;background-color:#f5f5f5;}
        .comment-action a{margin-left: 10px;}
        .comment-quote-wrap blockquote{padding: 5px;border: 1px solid #ccc;margin-bottom: 5px;}
        .comment-action{font-size: 13px;}
        .comment-reply-content{width: 100%;max-width: 100%}
        .comment-reply-action{margin: 5px 0;position: relative}
        .comment-reply-action>span{position: absolute;left: 0}
        .comment-reply-action label{margin-right: 10px}
        .comment-tip{font-weight: bold;color: red;position: absolute;display: none;opacity: 0;}
        
        .comment-add{margin-top: 30px;margin-bottom: 50px}
        .comment-add textarea{width: 100%;min-height: 50px;height: auto}
        .comment-add button{margin: 20px}
        .comment-pagination a,.comment-pagination span{
        	padding: 8px 15px;
        	border: 1px solid #ccc;
        	margin: 5px;
        	font-weight: bold;
        	display: inline-block;
        }
        .comment-pagination .current{background-color: darkcyan}
        
    </style>
</head>
<body>
<div class="container">
	
    <h1 class="page-header text-center">嵌套评论</h1>
    <div id="comment-list">
    <?php if (!empty($buildingList)):?>
   
    <?php foreach ($buildingList as $building):?>
    <?php $floorId = array_pop($building['floors'])?>
    <div class="row comment-item" data-building-id="<?php echo $building['id']?>">
        <div class="col-md-12 comment-main-header">
            <h5 class="">
                <span class="comment-username"><?php echo $building['position']?>楼</span>
                <span class="comment-username"><?php echo $floorList[$floorId]['user_name']?></span>
                <span class="comment-address text-muted">来自[<?php echo $floorList[$floorId]['address']?>]</span>
                <span class="comment-time pull-right text-muted">发表于<?php echo date('Y-m-d H:i:s', $floorList[$floorId]['add_time'])?></span>
            </h5>
        </div>
       
        <div class="col-md-1 comment-avatar">
            <a href="#"><img src="/Public/images/1.jpg" width="80" /></a>
        </div>
        <div class="col-md-11 comment-body">
            <div class="comment-quote-wrap">
                <?php echo !empty($building['floors']) ? renderQuote2($building['floors'], $floorList) : ''?>
            </div>
            <div class="comment-text lead"><?php echo $floorList[$floorId]['content']?></div>
            <div class="comment-action clearfix" data-floor-id="<?php echo $floorList[$floorId]['id']?>">
                <div class="pull-right">
                    <a class="comment-up" href="#">支持(<span class="comment-up-count"><?php echo $floorList[$floorId]['up_count']?></span>)</a>
                    <a class="comment-down" href="#">反对(<span class="comment-down-count"><?php echo $floorList[$floorId]['down_count']?></span>)</a>
                    <a href="#" class="comment-report">举报</a>
                    <a href="#" class="comment-reply">回复</a>
                </div>
            </div>
        </div>
    </div>
    <?php endForeach?>
    
    <?php endIF?>
    </div>
    <div class="row comment-pagination">
    	<div class="col-md-offset-3 col-md-6 text-center">
    		<?php echo $pagination?>
    	</div>
    </div>
    
    <div class="row comment-add">
    	<div class="col-md-offset-3 col-md-6">
    		<textarea maxLength="200"></textarea>
    	</div>
    	<div class="col-md-offset-3 col-md-6 text-center" style="position: relative">
    		<span class="pull-left" style="margin-top: 20px;position: absolute;left: 0;margin-left:15px">
	    		<label>用户名：</label>
	    		<input type="text" name="username" placeholder="留空为匿名用户" maxLength="20"/>
    		</span>
    		<button class="btn btn-primary" id="add-building">提交</button>
    	</div>
    </div>
</div>
<button class="btn btn-default comment-tip">+ 1</button>

<script type="text/javascript">
   const REPLY = '<div class="reply clearfix"><textarea class="comment-reply-content" placeholder="请输入评论内容。再次点击回复取消回复框"></textarea><div class="comment-reply-action text-center"><span><label>用户名</label><input type="text" name="username" placeholder="留空为匿名用户"/></span><button class="btn btn-info btn-comment-append">回复</button></div></div>';
	var $commentList = $('#comment-list');

   var $replyHtml = $(REPLY);
   var show = false, btn = null;
   $commentList.on("click", '.comment-reply', function(e) {
        e.preventDefault();
        var $btn = $(this);
        if (!show || btn !== this) {
            //没显示，或者点的不是自己
            btn = this;
            show = true;
            $btn.closest('.comment-action').append($replyHtml);
            $replyHtml.find('textarea').focus();
            $replyHtml.find('.btn-comment-append').attr('data-parentid', $btn.closest('.comment-action').attr('data-floor-id'));
        } else if (btn === this) {
            //显示了又点自己
            show = false;
            $replyHtml.detach().find('btn-comment-append').text('回复');
        }
        
   });
   //追加楼层
   $replyHtml.on("click", '.btn-comment-append', function(e) {
	   var $btn = $(this);
	   var parentId = $btn.attr('data-parentid');
	   var $content = $btn.parent().prev();
	   var content = $content.val();
	   var username = $btn.prev().find('input[name=username]').val();
	   if ($.trim(content) === '') {
			return;
		}
		if (username.length > 20) {
			alert('用户名过长');
			return;
		}
		if (content.length > 200) {
			alert('内容过多');
			return;
		}
	   $.ajax({
			url: '/index/addBuilding',
			type: 'POST',
			dataType: 'json',
			data: 'content=' + encodeURIComponent(content.trim()) + '&parentFloorId=' + parentId + '&username=' + encodeURIComponent(username.trim()),
			timeout: 8000,
			beforeSend: function() {
				$btn.attr('disabled', 'disabled').text('发表中...');
			}
		}).done(function(result) {
			$btn.removeAttr('disabled');
			if (result.code == 1) {
				$commentList.append(result.data);
				$btn.text('回复');
				$content.val('');
				show = false;
		        $replyHtml.detach();
		        var H = document.documentElement.scrollHeight;
		        $(document).scrollTop(H - window.innerHeight);
			} else {
				$btn.text(result.msg);
				console.log(result);
			}
		}).error(function(xhr, errorText) {
			$btn.removeAttr('disabled');
			$btn.text(errorText);
		})
	});

   //支持与反对
   var $up = $('.comment-tip:first');
   var flying = false;
   $commentList.on("click", '.comment-up,.comment-down', function(e) {
        e.preventDefault();
        if (flying) {
            return;
        }
        var $btn = $(this);
        if ($btn.hasClass('comment-up')) {
			$up.text('+ 1');
			var value=1;
			var className = '.comment-up-count';
        } else {
        	$up.text('- 1');
			var value=-1;
			var className = '.comment-down-count';
        }
        var floorId = $btn.closest('.comment-action').attr('data-floor-id');
        $.ajax({
			url: '/Index/vote',
			type: 'post',
			dataType: 'json',
			data: 'value=' + value + '&floorId=' + floorId,
        }).done(function(result) {
			console.log(result);
			var offset = $btn.offset();
	        flying = true;
	        if (result.code == 1) {
		        $btn.find(className).text(function(index, text) {return parseInt(text)+1});
		        $up.css({
		            left: offset.left,
		            top: offset.top - $up.height() - $btn.height(),
		        }).show().animate({top: "-=20px", opacity: 1}).delay(500).fadeOut(function(){
		            $up.removeAttr("style");
		            flying = false;
		        });
	        }
        }).error(function(xhr, errorText) {
        	flying = true;
			console.log(errorText);
        });
        

   });

   //添加楼栋
   var $addBuildingBtn = $('#add-building');
   $addBuildingBtn.click(function(e) {
	   var content = $('.comment-add textarea').val();
	   var username = $('.comment-add input[name=username]').val();
	   if (content.trim() === '') {
			return;
		}
	   if (username.length > 20) {
			alert('用户名过长');
			return;
		}
		if (content.length > 200) {
			alert('内容过多');
			return;
		}
		$.ajax({
			url: '/index/addBuilding',
			type: 'POST',
			dataType: 'json',
			data: 'content=' + encodeURIComponent(content.trim()) + '&username=' + encodeURIComponent(username.trim()),
			timeout: 8000,
			beforeSend: function() {
				$addBuildingBtn.attr('disabled', 'disabled').text('发表中...');
			}
		}).done(function(result) {
			$addBuildingBtn.removeAttr('disabled');
			if (result.code == 1) {
				$('#comment-list').append(result.data);
				$addBuildingBtn.text('发表');
				$('.comment-add textarea').val('');
			} else {
				$addBuildingBtn.text(result.msg);
				console.log(result);
			}
		}).error(function(xhr, errorText) {
			$addBuildingBtn.removeAttr('disabled');
			$addBuildingBtn.text(errorText);
		})
   });



</script>
</body>
</html>