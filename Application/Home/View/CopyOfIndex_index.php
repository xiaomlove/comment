<!DOCTYPE HTML>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>嵌套评论</title>
    <script type="text/javascript" src="http://libs.baidu.com/jquery/1.9.0/jquery.js"></script>
    <link rel="stylesheet" href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css"/>
    <script type="text/javascript" src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <style type="text/css">
        .lead{margin-bottom: 0;}
        .comment-main-header{background-color:#f5f5f5;border-top: 1px solid #ddd;margin-bottom: 10px;}
        .comment-main-header span:not(:last-of-type){margin-right: 10px;}
        .comment-item{padding-bottom: 20px;background-color: hsl(85,123%,205%) !important}
        .comment-header{font-size: 13px;}
        .comment-header span:not(:last-of-type){margin-right: 10px;}
        .comment-item>div>.comment-header{margin-top: 0;padding: 10px 0;background-color:#f5f5f5;}
        .comment-action a{margin-left: 10px;}
        .comment-quote-wrap blockquote{padding: 5px;border: 1px solid #ccc;margin-bottom: 5px;}
        .comment-action{font-size: 13px;}
        .comment-reply-content{width: 100%;max-width: 100%}
        .comment-reply-action{margin: 5px 0;}
        .comment-tip{font-weight: bold;color: red;position: absolute;display: none;opacity: 0;}
        
        .comment-add{margin-top: 30px;margin-bottom: 50px}
        .comment-add textarea{width: 100%;min-height: 50px;}
        .comment-add button{margin: 20px}
    </style>
</head>
<body>
<div class="container">
    <h1 class="page-header">嵌套评论</h1>
    <div id="comment-list">
    <div class="row comment-item">
        <div class="col-md-12 comment-main-header">
            <h5 class="">
                <span class="comment-username">1楼</span>
                <span class="comment-username">张三</span>
                <span class="comment-address text-muted">来自[湖南张家界]</span>
                <span class="comment-time pull-right text-muted">发表于 2015-07-08 09:21:56</span>
            </h5>
        </div>
       
        <div class="col-md-1 comment-avatar">
            <a href="#"><img src="Public/images/1.jpg" width="80" /></a>
        </div>
        <div class="col-md-11 comment-body">
            <div class="comment-quote-wrap">
                <blockquote class="comment-quote">
                    <blockquote class="comment-quote">
                        <blockquote class="comment-quote">
                            <div class="comment-info clearfix">
                                <h5 class="comment-header text-muted">
                                    <span class="comment-username">张三</span>
                                    <span class="comment-address">来自[湖南张家界]</span>
                                    <span class="comment-floor pull-right">#1</span>
                                </h5>
                                <div class="comment-text lead">大家不要吵了！都安静一下！！！给我刘日天一个面子！！！！！！</div>
                                <div class="comment-action clearfix">
                                    <div class="pull-right">
                                        <a class="comment-up" href="#">支持(<span class="comment-up-count">14</span>)</a>
                                        <a class="comment-down" href="#">反对(<span class="comment-down-count">42</span>)</a>
                                        <a href="#" class="comment-report">举报</a>
                                        <a href="#" class="comment-reply">回复</a>
                                    </div>
                                </div>
                            </div>
                        </blockquote>
                        <div class="comment-info clearfix">
                            <h5 class="comment-header text-muted">
                                <span class="comment-username">张三</span>
                                <span class="comment-address">来自[湖南张家界]</span>
                                <span class="comment-floor pull-right">#2</span>
                            </h5>
                            <div class="comment-text lead">大家不要吵了！都安静一下！！！给我刘日天一个面子！！！！！！</div>
                            <div class="comment-action clearfix">
                                <div class="pull-right">
                                    <a class="comment-up" href="#">支持(<span class="comment-up-count">14</span>)</a>
                                    <a class="comment-down" href="#">反对(<span class="comment-down-count">42</span>)</a>
                                    <a href="#" class="comment-report">举报</a>
                                    <a href="#" class="comment-reply">回复</a>
                                </div>
                            </div>
                        </div>
                    </blockquote>
                    <div class="comment-info clearfix">
                        <h5 class="comment-header text-muted">
                            <span class="comment-username">张三</span>
                            <span class="comment-address">来自[湖南张家界]</span>
                            <span class="comment-floor pull-right">#3</span>
                        </h5>
                        <div class="comment-text lead">大家不要吵了！都安静一下！！！给我刘日天一个面子！！！！！！</div>
                        <div class="comment-action clearfix">
                            <div class="pull-right">
                                <a class="comment-up" href="#">支持(<span class="comment-up-count">14</span>)</a>
                                <a class="comment-down" href="#">反对(<span class="comment-down-count">42</span>)</a>
                                <a href="#" class="comment-report">举报</a>
                                <a href="#" class="comment-reply">回复</a>
                            </div>
                        </div>
                    </div>
                </blockquote>
            </div>
            <div class="comment-text lead">大家不要吵了！都安静一下！！！给我刘日天一个面子！！！！！！</div>
            <div class="comment-action clearfix">
                <div class="pull-right">
                    <a class="comment-up" href="#">支持(<span class="comment-up-count">14</span>)</a>
                    <a class="comment-down" href="#">反对(<span class="comment-down-count">42</span>)</a>
                    <a href="#" class="comment-report">举报</a>
                    <a href="#" class="comment-reply">回复</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row comment-item">
        <div class="col-md-12  comment-main-header">
            <h5 class="">
                <span class="comment-username">2楼</span>
                <span class="comment-username">张三</span>
                <span class="comment-address text-muted">来自[湖南张家界]</span>
                <span class="comment-time pull-right text-muted">发表于 2015-07-08 09:21:56</span>
            </h5>
        </div>
       
        <div class="col-md-1 comment-avatar">
            <a href="#"><img src="Public/images/1.jpg" width="80" /></a>
        </div>
        <div class="col-md-11 comment-body">
            <div class="comment-quote-wrap">
                <blockquote class="comment-quote">
                    <blockquote class="comment-quote">
                        <div class="comment-info clearfix">
                            <h5 class="comment-header text-muted">
                                <span class="comment-username">张三</span>
                                <span class="comment-address">来自[湖南张家界]</span>
                                <span class="comment-floor pull-right">#1</span>
                            </h5>
                            <div class="comment-text lead">大家不要吵了！都安静一下！！！给我刘日天一个面子！！！！！！</div>
                            <div class="comment-action clearfix">
                                <div class="pull-right">
                                    <a class="comment-up" href="#">支持(<span class="comment-up-count">14</span>)</a>
                                    <a class="comment-down" href="#">反对(<span class="comment-down-count">42</span>)</a>
                                    <a href="#" class="comment-report">举报</a>
                                    <a href="#" class="comment-reply">回复</a>
                                </div>
                            </div>
                        </div>
                    </blockquote>
                    <div class="comment-info clearfix">
                        <h5 class="comment-header text-muted">
                            <span class="comment-username">张三</span>
                            <span class="comment-address">来自[湖南张家界]</span>
                            <span class="comment-floor pull-right">#2</span>
                        </h5>
                        <div class="comment-text lead">大家不要吵了！都安静一下！！！给我刘日天一个面子！！！！！！</div>
                        <div class="comment-action clearfix">
                            <div class="pull-right">
                                <a class="comment-up" href="#">支持(<span class="comment-up-count">14</span>)</a>
                                <a class="comment-down" href="#">反对(<span class="comment-down-count">42</span>)</a>
                                <a href="#" class="comment-report">举报</a>
                                <a href="#" class="comment-reply">回复</a>
                            </div>
                        </div>
                    </div>
                </blockquote>
            </div>
            <div class="comment-text lead">大家不要吵了！都安静一下！！！给我刘日天一个面子！！！！！！</div>
            <div class="comment-action clearfix">
                <div class="pull-right">
                    <a class="comment-up" href="#">支持(<span class="comment-up-count">14</span>)</a>
                    <a class="comment-down" href="#">反对(<span class="comment-down-count">42</span>)</a>
                    <a href="#" class="comment-report">举报</a>
                    <a href="#" class="comment-reply">回复</a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="row comment-add">
    	<div class="col-md-offset-3 col-md-6">
    		<textarea></textarea>
    	</div>
    	<div class="col-md-offset-3 col-md-6 text-center">
    		<button class="btn btn-primary" id="add-building">提交</button>
    	</div>
    </div>
</div>
<button class="btn btn-default comment-tip">+ 1</button>

<script type="text/javascript">
   const REPLY = '<div class="reply clearfix"><textarea class="comment-reply-content" placeholder="请输入评论内容..."></textarea><div class="comment-reply-action text-center"><button class="btn btn-primary btn-xs">回复</button></div></div>';
   var $replyHtml = $(REPLY);
   var show = false, btn = null;
   $('a.comment-reply').click(function(e) {
        e.preventDefault();
        var $btn = $(this);
        if (!show || btn !== this) {
            //没显示，或者点的不是自己
            btn = this;
            show = true;
            $btn.closest('.comment-action').append($replyHtml);
            $replyHtml.find('textarea').focus();
        } else if (btn === this) {
            //显示了又点自己
            show = false;
            $replyHtml.detach();
        }
        
   });

   //支持与反对
   var $up = $('.comment-tip:first');
   var flying = false;
   $('a.comment-up').add($('a.comment-down')).click(function(e) {
        e.preventDefault();
        if (flying) {
            return;
        }
        var $btn = $(this);
        $btn.hasClass('comment-up') ? $up.text('+ 1') : $up.text('- 1');
        var offset = $btn.offset();
        flying = true;
        $up.css({
            left: offset.left,
            top: offset.top - $up.height() - $btn.height(),
        }).show().animate({top: "-=20px", opacity: 1}).delay(500).fadeOut(function(){
            $up.removeAttr("style");
            flying = false;
        });

   });

   //添加楼栋
   var $addBuildingBtn = $('#add-building');
   $addBuildingBtn.click(function(e) {
	   var content = $('.comment-add textarea').val();
	   if (content.trim() === '') {
			return;
		}
		$.ajax({
			url: '/index/addBuilding',
			type: 'POST',
			dataType: 'json',
			data: 'content=' + encodeURIComponent(content),
		}).done(function(result) {
			console.log(result);
		})
   })

</script>
</body>
</html>