<?php
namespace Home\Controller;
use Think\Controller;

class IndexController extends Controller 
{
	
    public function index()
    {
    	$get = I('get.');
    	$page = empty($get['p']) ? 1 : $get['p'];
    	$per = 10;
    	$offset = ($page - 1)*$per;
    	$D_CommentBuilding = D('CommentBuilding');
    	$D_CommentFloor = D('CommentFloor');
    	$buildlingList = $D_CommentBuilding->order('position ASC')->limit("$offset,$per")->select();
    	$total = $D_CommentBuilding->count();
    	if (!empty($buildlingList))
    	{
    		$floorStr = '';
    		foreach ($buildlingList as &$building)
    		{
    			$floorStr .= $building['floors'].',';
    			$building['floors'] = explode(',', $building['floors']);
    		}
    		unset($building);
    		reset($buildlingList);
    		
    		$floorIdArr = array_unique(explode(',', trim($floorStr, ',')));
    		$floorList = $D_CommentFloor->where(array('id' => array('in', $floorIdArr)))->select();
    		$floorListIdKey = array();
    		foreach ($floorList as &$floor)
    		{
    			$floorListIdKey[$floor['id']] = $floor;
    		}
    		unset($floor);
    		unset($floorList);
    		
//     		foreach ($buildlingList as &$building)
//     		{
    			
//     		}
    		
//     		dump($floorListIdKey);
//     		dump($buildlingList);
			$this->assign('buildingList', $buildlingList);
			$this->assign('floorList', $floorListIdKey);
    	}
    	$pageClass = new \Think\Page($total, $per);
    	$pagination = $pageClass->show();
    	$this->assign('pagination', $pagination); 
    	$this->display();
    }
    
    
    public function addBuilding()
    {
//     	dump(!IS_AJAX);
//     	dump(!IS_POST);
//     	dump(!IS_AJAX || !IS_POST);exit;
    	$s = $_SERVER;
    	if (!IS_AJAX || !IS_POST)
    	{
    		dump((!IS_AJAX || !IS_POST));
    		$this->ajaxReturn(array('code' => -1, 'msg' => '非法访问'));
    	}
    	if (!empty($_SERVER['HTTP_HOST']) && $s['HTTP_HOST'] !== 'comment.tinyhd.net')
    	{
    		$this->ajaxReturn(array('code' => -1, 'msg' => '非法访问', 'HTTP_HOST' => $s['HTTP_HOST']));
    	}
    	if (!empty($_SERVER['HTTP_ORIGIN']) && $s['HTTP_ORIGIN'] !== 'http://comment.tinyhd.net')
    	{
    		$this->ajaxReturn(array('code' => -1, 'msg' => '非法访问', 'HTTP_ORIGIN' => $s['HTTP_ORIGIN']));
    	}
    	if (!empty($_SERVER['HTTP_REFERER']) && substr($s['HTTP_REFERER'], 0, 26) !== 'http://comment.tinyhd.net/')
    	{
    		$this->ajaxReturn(array('code' => -1, 'msg' => '非法访问', 'HTTP_REFERER' => substr($s['HTTP_REFERER'], 0, 26)));
    	}
    	
    	$content = trim(I('post.content'));
    	if (empty($content))
    	{
    		$this->ajaxReturn(array('code' => -1, 'msg' => '非法访问', 'NO CONTENT'));
    	}
		$userName = htmlspecialchars(I('username'));
		$content = nl2br(htmlspecialchars($content));
		$parentFloorId = I('parentFloorId');
		$D_CommentFloor = D('CommentFloor');
		$floorPath = '/';
		$floorPosition = 1;//楼层的层数
		$buildFloors = NULL;
		if (!empty($parentFloorId))
		{
			$parentFloorInfo = $D_CommentFloor->where(array('id' => $parentFloorId))->find();
			if (empty($parentFloorInfo))
			{
				$this->ajaxReturn(array('code' => -1, 'msg' => '父楼层不存在'));
			}
			$floorPath = $parentFloorInfo['path'].$parentFloorId.'/';
			$floorPosition = $parentFloorInfo['position'] + 1;
			$buildFloors = trim(str_replace('/', ',', $floorPath), ',');
		}
		$floorId = self::addFloor($userName, $content, $floorPath, $floorPosition);
		if (empty($floorId))
		{
			$this->ajaxReturn(array('code' => -1, 'msg' => '添加楼层失败'));
		}
		if (empty($buildFloors))
		{
			$buildFloors = $floorId;
		}
		else
		{
			$buildFloors .= ','.$floorId;
		}
    	$building = D('CommentBuilding');
    	$buildPosition = $building->getMaxPosition();//楼栋的层数
    	$data = array(
    			'position' => $buildPosition + 1,
    			'add_time' => NOW_TIME,
    			'floors' => $buildFloors,
    	);
    	
    	$add = $building->add($data);
    	if ($add)
    	{
    		//取出这一栋的信息
    		$buildFloors = explode(',', $buildFloors);
    		$floorList = $D_CommentFloor->where(array('id' => array('in', $buildFloors)))->order('position ASC')->select();
    		$floorListIdKey = array();
    		foreach ($floorList as &$floor)
    		{
    			$floorListIdKey[$floor['id']] = $floor;
    		}
    		unset($floor);
    		unset($floorList);
			
    		$data['floors'] = $buildFloors;
    		$data['id'] = $add;
    		$this->assign('building', $data);
    		$this->assign('floorList', $floorListIdKey);
    		
    		$result = $this->fetch('Index_index_building');
//     		dump($result);exit;
    		$this->ajaxReturn(array('code' => 1, 'msg' => '添加楼栋成功', 'data' => $result));
    	}
    	else
    	{
    		$this->ajaxReturn(array('code' => -1, 'msg' => '添加楼栋失败:'.$building->getLastSql()));
    	}
    }
    /**
     * 每一个回复都是一个楼层，也都产生一个楼栋，先添加楼层数据，再添加楼栋。
     * 独立的回复，新建一栋楼，它的floors只有它自己
     * 回复一栋楼中间或末尾，也都是新建一栋楼，它的floors是回复的楼层作为末层时楼栋的floors加上它自己
     * 
     * [楼层]的path段是它的八父楼层，以/分割，如果是顶层，则为/
     * [楼栋]的floors则是最末层的path+最末层的id，将'/'转换为为','并去掉左的','存储
     */
    public function addFloor($userName, $content, $path = '/', $position = 1)
    {
    	$floor = D('CommentFloor');
    	if (empty($userName))
    	{
    		$userName = '匿名用户';
    	}
    	if (empty($content))
    	{
    		$content = '测试的空回复';
    	}
 
		$data = array(
				'user_name' => $userName,
				'address' => getAddress('country'),
				'add_time' => NOW_TIME,
				'content' => $content,
				'path' => $path,
				'position' => $position,
		);
    	$add = $floor->add($data);
    	return $add;
    }
    
    public function vote()
    {
    	$value = I('value');
    	$floorId = I('floorId');
    	if (empty($value) || empty($floorId))
    	{
    		$this->ajaxReturn(array('code' => -1, 'msg' => '参数错误'));
    	}
    	$D_CommentFloor = D('Comment_floor');
    	if ($value == 1)
    	{
    		$vote = $D_CommentFloor->where(array('id' => $floorId))->setInc('up_count');
    	}
    	else
    	{
    		$vote = $D_CommentFloor->where(array('id' => $floorId))->setInc('down_count');
    	}
    	if ($value)
    	{
    		$this->ajaxReturn(array('code' => 1, 'msg' => '表态成功'));
    	}
    	else
    	{
    		$this->ajaxReturn(array('code' => -1, 'msg' => '表态失败'));
    	}
    }
    
   
}