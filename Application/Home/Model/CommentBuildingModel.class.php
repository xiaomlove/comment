<?php
namespace Home\Model;

use Think\Model;

class CommentBuildingModel extends Model
{
	protected $tableName = 'comment_building';
	
	public function getMaxPosition()
	{
		$position = $this->max('position');
		return empty($position) ? 0 : $position;
	}
}