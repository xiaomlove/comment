<?php
return array(
		'HTML_CACHE_ON' => false,
// 		'SHOW_PAGE_TRACE' => true,
		'MULTI_MODULE' => false,
		'DEFAULT_MODULE' => 'Home',
// 		'DEFAULT_C_LAYER' => 'Action',
// 		'ACTION_BIND_CLASS' => true,
		'TMPL_FILE_DEPR' => '_',
// 		'URL_ROUTER_ON' => true,
		'URL_ROUTE_RULES' => array(
			'xxx' => 'Index/xxx',
		),
// 		'APP_GROUP_LIST' => 'Home,Admin',
		
		
		'TMPL_ENGINE_TYPE' =>'PHP',
		'TMPL_TEMPLATE_SUFFIX' => '.php',
		
		'DB_TYPE'   => 'mysql', // 数据库类型
		'DB_HOST'   => '115.28.132.38', // 服务器地址
		'DB_NAME'   => 'test', // 数据库名
		'DB_USER'   => 'root', // 用户名
		'DB_PWD'    => '123456', // 密码
		'DB_PORT'   => 3306, // 端口
		'DB_PREFIX' => '', // 数据库表前缀 
		'DB_CHARSET'=> 'utf8', // 字符集
		'DB_DEBUG'  =>  TRUE, // 数据库调试模式 开
		
);